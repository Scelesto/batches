module={
  name:'chart',
  dependencies:['jq','d3','randcolor'],
  proto:function(type,elem,data){
    return this.chart(type,elem,data);
  },
  val:function(){
    this.basicChart=function(element,data){
      //default settings for all charts go here
      this.element=element;
      this.data=data;
    };
    this.createChart=function(child){
      var _this=this;
      return function(){
        _this.basicChart.apply(this,arguments);
        child.apply(this,arguments);
      }
    };
    this.barChart=this.createChart(function(element,question){
      $(element).html('<div class="chart"></div><div class="percentages"></div>');
      var data=[],labels=[];
      question.responses.map(function(d){
        data.push(d.float);
        labels.push(d.value);
      })
      var colors=multiple_random_color_generation(data.length),
      x=d3.scale.linear()
        .domain([0, d3.max(data)])
        .range([0, 100]),
      y=d3.scale.linear()
        .domain([0, d3.sum(data)])
        .range([0, 100]);
      d3.select(element).select(".chart")
        .selectAll("div")
        .data(data).enter()
          .append("div")
          .style({width:function(d){return x(d)+"%"}})
          .style({'background-color':function(d,i){return colors[i]}})
          .attr('data-text-content',function(d,i){
            return labels[i];
          });
      d3.select(element).select(".percentages")
        .selectAll("div")
        .data(data).enter()
          .append("div")
          .text(function(d){return y(d).toFixed(1)+"%"});
    });
    this.lineChart=this.createChart(function(question){

    });
    this.chartTypes={'bar':this.barChart,'line':this.lineChart}
    this.chart=function(type,elem,data){
      if(!elem){return false}
      for(var chartType in this.chartTypes){
        if(type==chartType){
          return new this.chartTypes[chartType](elem,data);
        }
      }
    };
  }
}
