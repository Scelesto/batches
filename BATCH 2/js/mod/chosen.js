/*
U.js module implementing jQuery's Chosen extension for searching.
*/
module={
	name:'chosen',
	dependencies:['question','jq','chosen'],
	proto:function(selector,object){
		if(selector.constructor==String){
			var t=this;
			$(selector).each(function(){
				t.handle($(this),object);
			});
			return;
		}
		return this.handle(selector,object);
	},
	val:function(){
		$("<link/>", {rel: "stylesheet",type: "text/css",href: "css/chosen.css"}).appendTo("head");
		this.chosenMod=function(chosen){
			chosen.result_add_option=this.chosen_result_prototype.bind(chosen);
			chosen.winnow_results=this.chosen_winnow_results.bind(chosen);
			if(!this.chosen_constructor){this.chosen_constructor=chosen.constructor}
			//fix placeholder bug
			setTimeout(chosen.search_field_scale.bind(chosen),0);
			return chosen;
		}
		var chosenProto=function(element){
			this.$element=element;
			this.$chosen=element.data().chosen;
			this.questionHandler=U('question').reference({name:'chosen',search_loc:element.data('search-loc')});
			this.update=function(){
				this.$chosen.test='asdf';
				var oldResults=this.$chosen.results_data,opt=this.$chosen.form_field.options,newresults=oldResults,oldResults=oldResults.map(function(e){return e.value}),r,q,myQuestions=this.questionHandler.getMyQuestions();
				for(i=0;i<myQuestions.length;i++){
					r=newresults.length;
					q=myQuestions[i].question;
					if(oldResults.indexOf(q)!=-1){continue}
					newresults[r]={array_index:r,options_index:r,
						html:q,search_text:q,text:q,value:q,
						classes:"",disabled:false,search_match:false,selected:false,style:""};
					opt[r]=$('<option value="'+q.replace(/["']/g,'\\$&')+'"></option>')[0];
				}
				this.$chosen.results_data=newresults;
			}
			this.addQuestions=function(questions){
				for(var i=0;i<questions.length;i++){
					this.questionHandler.addQuestion(questions[i]);
				}
			}
			this.getQuestions=function(callback){
				this.questionHandler.search(this.$chosen.get_search_text(),(function(questions){
					callback.call(this);
				}).bind(this));
			}
		},chosenObject=function(element){
			return new chosenProto(element);
		},__getChosenByElement__=function(test,element){
			return allChosenObjs[allChosenObjs.map(function(e){return e[test]}).indexOf(element)]
		},__getChosen__=function(e){
			return [//e.constructor,testfunction,arguments...
			[this.chosen_constructor,__getChosenByElement__,'$chosen',e],
			[$,__getChosenByElement__,'$element',e],
			[Number,function(){return allChosenObjs[e]}],
			[Function,allChosenObjs.reduce,function(a,b){return a||e(b)?b:a},undefined]
			].reduce(function(a,b){
				return a||(e.constructor==b[0]?b[1](b[2],b[3]):a)
			},undefined)
		},__newChosen__=function(element){
			var chosen=chosenObject(element);
			chosen.$element.next().css('width','100%');
			chosen.id=allChosenObjs.length;
			allChosenObjs.push(chosen);
			return chosen
		},allChosenObjs=[];
		this.handle=function(o,object){
			o=o.constructor==String?$(o):o;
			var get=__getChosen__(o);
			if(get){return get}
			o=typeof(object)=='undefined'?o:o.chosen(object);
			o=typeof(this.chosen_constructor)=='undefined'?o:o.constructor==this.chosen_constructor?o.container.prev():o;
			this.chosenMod(o.data().chosen);
			return __newChosen__(o);
		}
		this.__init=function(){ //on initialization, extend chosen fork
			$('<link/>',{rel:"stylesheet",type:"text/css",href:"https://harvesthq.github.io/chosen/chosen.css"}).appendTo('head');
			this.chosen_winnow_results=function(){
				if (this.search_field.val().replace(/\s/g,'').length < 3) {
					return this.results_hide()
				}
				U('chosen').handle(this.container.prev()).update();
				var escapedSearchText, option, regex, results, results_group, searchText, startpos, text, text2, zregex, newzreg, foundtxt, _i, _len, _ref;
				this.no_results_clear();
				results = 0;
				searchText = this.get_search_text();
				escapedSearchText = searchText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
				var est2 = escapedSearchText.replace(/\\\ .*/, '');
				escapedSearchText = escapedSearchText.replace(/\\\ /g, '|');
				zregex = new RegExp(escapedSearchText, 'i');
				newzreg = new RegExp(escapedSearchText, 'ig');
				var newreg = new RegExp(est2, 'i');
				regex = this.get_search_regex(escapedSearchText);
				_ref = this.results_data;
				var absret = true;
				for (_i = 0, _len = _ref.length; _i < _len; _i++) {
					if(results==4){break}
					option = _ref[_i];
					option.search_match = false;
					results_group = null;
					option.search_text = option.group ? option.label : option.html;
					var STCLONE = option.search_text,
						newtest = newreg.test(STCLONE);
					absret = absret && !newtest;
					if (newtest && this.include_option_in_results(option)) {
						if (option.group) {
							option.group_match = false;
							option.active_options = 0;
						}
						if ((option.group_array_index != null) && this.results_data[option.group_array_index]) {
							results_group = this.results_data[option.group_array_index];
							if (results_group.active_options === 0 && results_group.search_match) {
								results += 1;
							}
							results_group.active_options += 1;
						}
						if (!(option.group && !this.group_search)) {
							option.search_match = this.search_string_match(option.search_text, regex);
							if (option.search_match && !option.group) {
								results += 1;
							}
							if (option.search_match) {
								if (searchText.length) {
									foundtxt = option.search_text.match(newzreg);
									text2 = option.search_text;
									var removed = 0,
										text = "",
										r = 0,
										justadded = false;
									for (var q = 0; q < foundtxt.length; q++) {
										startpos = text2.search(newzreg);
										removed = option.search_text.length - text2.length;
										if (foundtxt[q].length != 1 || (startpos<2 && justadded && option.search_text[removed]==' ')) {
											justadded = true;
											text = text.substr(0, removed + (9 * r)) + option.search_text.substr(removed, startpos) + '<em>' + option.search_text.substr(removed + startpos, foundtxt[q].length) + '</em>' + option.search_text.substr(removed + startpos + foundtxt[q].length);
											r++;
										} else {
											justadded = false;
											text = text.substr(0, removed + (9 * r)) + option.search_text.substr(removed);
										}
										text2 = text2.substr(startpos + foundtxt[q].length);
									}
									option.search_text = text.replace(/\<\/em\>\ \<em\>/g, ' ');
								}
								if (results_group != null) {
									results_group.group_match = true;
								}
							} else if ((option.group_array_index != null) && this.results_data[option.group_array_index].search_match) {
								option.search_match = true;
							}
						}
					}
				}
				this.result_clear_highlight();
				if ((results < 1 && searchText.length) || absret) {
					this.update_results_content("");
					return this.no_results(searchText);
				} else {
					this.update_results_content(this.results_option_build());
					return this.winnow_results_set_highlight();
				}
			}
			this.chosen_result_prototype=function(option){
				var classes, option_el,d=U('question').question(option.value);
				if (!option.search_match||!this.include_option_in_results(option)){return ''}
				classes = ['chosen_search_result',option.disabled?'disabled-result':'active-result'];
				if(d.public_data){classes.push('public_data')}
				if(option.classes!==""){classes.push(option.classes)}
				return this.outerHTML($('<li class="' + classes.join(' ') + '" data-option-array-index="' + option.array_index + '"><img src="' + d.image + '" height="50px" width="50px"><span><span class="question_text">' + option.search_text + '</span><br><span class="responses">' + d.responses.map(function(x){return x.value}).join('; ') + '.</span><span class="sample_size">Sample: ' + d.sample_size + '</span></span></li>')[0]);
			}
		}
	}
}
