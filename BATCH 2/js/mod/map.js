//map.js
module={
	name:'map',
	dependencies:['jq','maps'],
	callback:'content_loaded',
	val:function(){
    this.configureHeight=function(){
      $('#gp_map_canvas').css('height',$('#GP').parent().height()-$('#GP .tabs').height()-$('#GP .menus').height()-30);
    }
    this.clearmap=function(){
      this.map.data.forEach(function(e){U.map.map.data.remove(e)});
      this.loadedData=false;
    }
    this.newmap=function(){
      //make a new map
      var map = new google.maps.Map(document.getElementById('gp_map_canvas'), {
        zoom: 1,
        center: {
          lat: 35,
          lng: 0
        },
        disableDefaultUI: true //without all the maps controls
      }),z;
      this.map=map;
      var z=this.zoommap;
      google.maps.event.addListenerOnce(map, 'idle', function() {
        z();
      });
    }
    this.zoommap=function(bounds,center){
      if(!bounds){this.map.setOptions({minZoom:0})}
      this.map.fitBounds(bounds?bounds:new google.maps.LatLngBounds(new google.maps.LatLng(-70.833115, -177.672852), new google.maps.LatLng(84.3591, 175.295898)));
      z=this.map.getZoom()+(bounds?0:1);
      this.map.setOptions({
        zoom:z,
        center:center?center:{lat:35,lng:0}
      })
      if(!bounds){this.map.setOptions({minZoom:z})}
    }
    this.stylemap=function(){
      this.map.setOptions({
        zoomControl:true,
        styles: [
          //get rid of all text, roads, geography, etc.
          {
            featureType: "all",
            elementType: "all",
            stylers: [{
              visibility: "off"
            }]
          }
        ]
      });
    }
    this.rangecolor={set:["fee5d9", "fcae91", "fb6a4a", "cb181d"],dead:'777'};
    this.getData=function(file){
      this.loadedData=file;
      var raw, data, ref=this.rangecolor.set.length,_this=this;
      $.get(file, function(x) {
        if (typeof(x) == "string") {
          x = $.parseJSON(x)
        } //fix mac parse bug
        raw = x;
        //produce references based on the range of data in the dataset
        var _ar = Object.keys(x).map(function(k) {
            return x[k]
          }),
          min = Math.min.apply(null, _ar),
          max = Math.max.apply(null, _ar);
        //linear map function
        data = Object.keys(x).reduce(function(p, y) {
          p[y] = Math.floor((x[y] - min) / (max - min) * ref);
          return p;
        }, {});
        _this.data=data;
      });
    }
    this.getShapeFiles=function(){
      var map=this.map;
      //get country shapefiles and add them to the map
      $.get('countries.geo.json', function(x) {
        if (typeof(x) == "string") {
          x = $.parseJSON(x)
        } //fix mac parse bug
        map.data.addGeoJson(x);
      });
    }
    this.showHeat=function(){
      //method for this geojson file of country codes existing
      var __CODE__ = function(feature) {
        return feature.getId();
      },_this=this,color=this.rangecolor,ref=this.rangecolor.set.length;
      this.map.data.forEach(function(feature) {
        var info = __CODE__(feature);
        feature.setOwnProperty('_raw', raw.hasOwnProperty(info) ? raw[info] : null);
      });
      this.map.data.setStyle(function(feature) { //for each country on the map
        //defaults to border and dead fill
        var _t = feature.getProperty('_hov'),
          style = {
            fillOpacity: 1,
            fillColor: '#' + color.dead
          },
          info = __CODE__(feature);
        style.strokeWeight = _t ? 1 : 0;
        if (_this.data.hasOwnProperty(info)) { //if we have data for this country
          if (_this.data[info] == ref) {
            _this.data[info] -= 1
          } //fix max datavalue bug
          style.fillColor = '#' + color.set[_this.data[info]]; //set fill color
        }
        return style; //tell Google to render this country
      });
    }
    this.showLabels=function(){
      var _this=this;
      //onhover
      this.map.data.addListener('mouseover', function(e) {
        var marker = e.feature.getProperty('marker');
        if (marker) {
          marker.setMap(this.map)
        } else {
          e.feature.setProperty('marker', new google.maps.Marker({
            position: _this.featureCoos(e.feature),
            map: this.map
          }));
        }
        e.feature.setProperty('_hov', true);
      });
      this.map.data.addListener('mouseout', function(e) {
        e.feature.setProperty('_hov', false);
        e.feature.getProperty('marker').setMap(null)
      });
      this.map.data.addListener('click',function(e){
        _this.zoommap(_this.getBounds(e.feature),_this.featureCoos(e.feature));
      });
    }
    this._m=function(file){
      if(!this.map){
        this.configureHeight();
        this.newmap();
        this.zoommap();
        this.stylemap();
        this.getData(file);
        this.getShapeFiles();
        this.showHeat();
        this.showLabels();
      }else{
        this.configureHeight();
        if(this.loadedData!=file){
          this.clearmap();
          this.zoommap();
          this.stylemap();
          this.getData(file);
          this.getShapeFiles();
        }
      }
    }
		this.apiReady=false;
		this.content_loaded=function(){
			this.apiReady=true;
      if(U.core.openTab!='location'||!U.core.selectedQuestion||!U.core.selectedQuestion.global_json){
        return; //if not in view, do not attempt to load at this time
      }
      this._m(U.core.selectedQuestion.global_json);
		}
		this.featureCoos=function(e) {
			return (function(m, n) {
				return new google.maps.LatLng(m, n)
			}).apply(null, (function(m) {
				return (function(n) {
					return m.map(function(v, w, x) {
						return x[0].reduce(function(y, z, a) {
							return y + z[w] * x[1][a]
						}, 0) / n
					})
				})(m[1].reduce(function(y, z) {
					return y + z
				}))
			})((function(v) {
				return [0, 1].map(function(f) {
					return v.map(function(g) {
						return g[f]
					})
				})
			})((function(v) {
				return v.getType() == "MultiPolygon" ? v.getArray() : [v]
			})(e.getGeometry()).map(function(v) {
				return v.getArray().map(function(w) {
					return (function(x) {
						return ['lat', 'lng'].map(function(y) {
							return x.map(function(z) {
								return z[y]()
							})
						})
					})(w.getArray()).map(function(x, y, z) {
						return y == 0 ? [x, z[1]].map(function(a) {
							return a.reduce(function(b, c) {
								return b + c
							}) / a.length
						}) : x.length
					})
				})[0]
			}))))
		}
    this.getBounds=function(e){
      var l=0,q;
      e.getGeometry().getArray().map(function r(x){
        if(x.constructor==google.maps.LatLng){
          q=q?q:[[x.lat(),x.lng()],[x.lat(),x.lng()]];
          q=q.map(function(a,i){
            return a.map(function(b,j){
              return (!i?function(a,b){return a<b?a:b}:function(a,b){return a>b?a:b})(b,(!j?x.lat():x.lng()));
            });
          });
          return;
        }
        return x.getArray().map(r)
      });
      return (function(b){return new google.maps.LatLngBounds(new google.maps.LatLng(b[0][0],b[0][1]), new google.maps.LatLng(b[1][0],b[1][1]))})(q);
    }
    this.ONLYWATER=[
			{
				featureType:'all',elementType:'all',
				stylers:[{visibility:'off'}]
			},{
				featureType:'water',elementType:'all',
				stylers:[{visibility:'simplified'},
					{hue:'#1900ff'}]
			},{
				featureType:'water',elementType:'labels',
				stylers:[{visibility:'off'}]
			}
		]
	}
}
