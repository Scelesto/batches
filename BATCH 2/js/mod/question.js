/*
U.js module for handling questions.
Modules can create custom references to these questions,
which allows for a uniform connection to the server across all modules,
and prevents superfluous server calls.
*/
module={
	name:'question',
	dependencies:['jq'],
	val:function(){
		var defineProto=function(allowedProp,object){
			if(object.constructor==String&&arguments[2]){
				return this.__def__({object:arguments[2]})
			}
			if(allowedProp.constructor==Array){
				for(var i=0;i<allowedProp.length;i++){
					if(object.hasOwnProperty(allowedProp[i])){
						this[allowedProp[i]]=object[allowedProp[i]]
					}
				}
			}
			return true
		},__autoConfigure__=function(object,lookup,allowedProps){
			this.__def__=defineProto.bind(this,allowedProps);
			this.valueOf=function(){
				for(var r={},prop,i=0;i<allowedProps.length;i++){
					prop=allowedProps[i];
					if(this.hasOwnProperty(prop)){
						r[prop]=this[prop]
					}
				}
				return r
			}
			if(typeof(object)=='number'||!object.hasOwnProperty('index')){
				object=lookup(object)
			}
			if(object){
				return this.__def__(object)
			}
			return false
		},referenceProto=function(object){
			__autoConfigure__.call(this,object,__fillReferenceObject__,[
				'index',
				'name',
				'search_loc'
			]);
			var data={},questions=[],updateHandler=function(return_value){
				__updateReference__(this);
				return return_value;
			};
			this.getData=function(key){
				return data[key];
			}
			this.setData=function(key,value){
				data[key]=value;
				__updateReference__(this);
			}
			this.getMyQuestions=function(){
				for(var r=[],i=0;i<questions.length;i++){
					r.push(__getQuestion__(questions[i]))
				}
				return r
			}
			this.myQuestionsEncode=function(){
				var q=this.getMyQuestions(),r=[];
				for(var i=0;i<q.length;i++){
					r.push(q[i].questionID);
				}
				return r.join(':')
			}
			this.hasQuestion=function(q){
				return questions.indexOf(q)!=-1
			}
			this.addQuestion=function(q){
				if(q.constructor!=questionProto){
					q=__getQuestion__(q)||__newQuestion__(q);
				}
				return updateHandler(questions.indexOf(q.index)!=-1?false:(questions.push(q)&&false)||true);
			};
			this.removeQuestion=function(){
				return updateHandler(questions.indexOf(q.index)==-1?false:(questions.reduce(function(a,b){
					if(b!=q.index){a.push(b)}
					return a
				},{})&&false)||true);
			};
			this.search=function(text,callback){
				callback();
			};
			__updateReference__(this);
		},referenceCallback=function(){
			if(arguments[0].constructor==Object){
				for(key in arguments[0]){
					try{
						this.setData(key,arguments[0][key])
					}catch(e){
						return false
					}
				}
				return true
			}
			if(arguments[1]){
				try{
					this.setData(arguments[0],arguments[1])
				}catch(e){
					return false
				}
				return true
			}
			try{
				return this.getData(arguments[0])
			}catch(e){
				return undefined
			}
		},questionProto=function(object){
			__autoConfigure__.call(this,object,__fillQuestionObject__,[
				'index',
				'question',
				'sample_selection',
				'sample_size',
				'responses',
				'image',
				'public_data',
				'global_json'
			]);
			this.getMyReferences=__getReference__.bind(this);
			__updateQuestion__(this);
		},questionObject=function(object){
			return new questionProto(object)
		},referenceObject=function(object){
			return U.proto(new referenceProto(object),referenceCallback)
		},__fillQuestionObject__=function(object){
			if(!object.hasOwnProperty('index')){
				object.index=allQuestions.length;
				return object
			}
			return __getQuestion__(object).valueOf()
		},__fillReferenceObject__=function(object){
			if(!object.hasOwnProperty('index')){
				object.index=allReferences.length;
				return object
			}
			return __getReference__(object).valueOf()
		},isQuestionTest=function(q){
			return typeof(q)=='string'?function(i){
				return i.question==q;
			}:function(i){
				for(value in q){
					if(i[value]!=q[value]){
						return false
					}
				}
				return true
			}
		},__questionExists__=function(q){
			if(typeof(q)=='number'){
				return allQuestions.hasOwnProperty(q)
			}
			for(var test=isQuestionTest(q),i=0;i<allQuestions.length;i++){
				if(test(allQuestions[i])){return true}
			}
			return false
		},__getQuestion__=function(q){
			if(typeof(q)=='number'){
				return allQuestions[q]
			}
			for(var test=isQuestionTest.call(this,q),currentQuestion,i=0;i<allQuestions.length;i++){
				currentQuestion=allQuestions[i];
				if(test(currentQuestion)){return currentQuestion}
			}
			return false
		},__getReference__=function(r){
			if(typeof(r)=='number'){
				return allReferences[r]
			}
			var currentReference,i=0;
			if(r.constructor==questionProto){
				for(var returnArray=[];i<allReferences.length;i++){
					currentReference=allReferences[i];
					if(currentReference.hasQuestion(r.index)){returnArray.push(currentReference)}
				}
				return returnArray
			}
			for(;i<allReferences.length;i++){
				currentReference=allReferences[i];
				if(!function(){
					for(value in r){
						if(allReferences[i][value]!=r[value]){
							return false
						}
					}
					return true
				}()){return currentReference}
			}
			return false
		},__newQuestion__=function(q){
			if(typeof(q)!='object'){return false}
			if(questionObject(q) in allQuestions){return false}
			allQuestions.push(questionObject(q));
			return allQuestions[allQuestions.length-1];
		},__newReference__=function(q){
			allReferences.push(referenceObject(q));
			return allReferences[allReferences.length-1];
		},__updateQuestion__=function(question){
			allQuestions[question.index]=question;
		},__updateReference__=function(reference){
			allReferences[reference.index]=reference;
		},allQuestions=[],allReferences=[];
		window._aq=allQuestions;
		this.question=function(q){
			return __getQuestion__(q)||__newQuestion__(q);
		};
		this.reference=function(r){
			return __getReference__(r)||__newReference__(r);
		};
	}
}
