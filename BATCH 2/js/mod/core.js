module={
  name:'core',
  dependencies:['jq','css','usage','chart','conditions','svg','logic','map'],
  val:function(){
    window.preloadQuestions=[
      {
        question:"How did you vote in the 2012 US Presidential Election?",
        sample_size:4012,
        responses:[
          {
            value:"Obama",
            float:0.297
          },
          {
            value:"Romney",
            float:0.275
          },
          {
            value:"Other",
            float:0.01
          },
          {
            value:"Didn't Vote",
            float:0.418
          }
        ],
        image:'http://www2.pictures.zimbio.com/gi/Mitt+Romney+Barack+Obama+Obama+Romney+Square+kKoxln3fSnYl.jpg',
        public_data:true
      },
      {
        question:"How much does it rain where you live?",
        sample_size:10345,
        responses:[
          {
            value:"Not Very Much",
            float:4
          },
          {
            value:"A Small Amount",
            float:5
          },
          {
            value:"A Moderate Amount",
            float:3
          },
          {
            value:"A Good Deal",
            float:1.43
          },
        ],
        image:'http://www.jcpost.com/wp-content/uploads/2015/06/rain-1.jpg',
        public_data:false,
        global_json:'worldbank.json'
      }
    ];
    this.activatePreloadQuestion=function(q){
      U.chart('bar','#DVP .results',q);
      this.selectedQuestion=q;
      this.gp_update();
    }
    this.clearPreloadQuestion=function(){
      $('#DVP .results').html('')
      this.selectedQuestion=false;
      this.gp_update();
    }
    this.selectedQuestion=false;
    this.onload=function(){
      $(function(){
        U('chart').createChart();
        setTimeout(function(){$('[data-module-fill="core"]').removeClass('unloaded')},0);
      });
      U('chosen',function(){
        this('.chosen-select',{
          max_selected_options:1
        });
        for(var i=0;i<preloadQuestions.length;i++){
          preloadQuestions[i]=U.question.question(preloadQuestions[i]);
          this($('.chosen-select').first()).questionHandler.addQuestion(preloadQuestions[i])
        }
        $(".chosen-select").first().change(function(){
          if($(this).val()==null){return}
          U.core.activatePreloadQuestion(U.question.question($(this).val()[0]));
        });
      });
      //interactive js
      var TABDATA={
        qnet:{
          menu:{
            rem_qst:{

            },cln_qst:{

            },lss_lnk:{

            },mre_lnk:{

            },edt_lbl:{

            },ans_qst:{

            },hde_box:{

            },hde_lnk:{

            },clr_pnl:{

            },org_box:{

            },shw_trc:{

            },fnc_qst:{

            }
          },sidebar:{
            add_qst:{

            },
            spc_trk:{

            }
          },def:[
            'clr_pnl',
            'add_qst',
            'spc_trk'
          ],click:function(){

          }
        },logic:{
          def:[]
        },time:{
          def:[]
        },sex:{
          def:[]
        },age:{
          def:[]
        },location:{
          menu:{
            res_inc:{

            },res_dec:{
              click:U.map.zoommap
            },col_all:{

            },col_eac:{

            },col_maj:{

            }
          },
          def:['col_all','col_eac','col_maj','res_inc','res_dec'],
          click:U.map.content_loaded
        },settings:{
          def:[]
        }
      },
      vis=function(){
        $('.menus').css('display','');
        var i=$(this).attr('id'),n=i.split('_')[0],d=TABDATA[n],m=$('#'+n+'_menu'),p=$('#'+n+'_panel'),s=$('#'+n+'_sidebar');
        $('.menus>div').css('display','none');$('.panels>div').css('display','none');
        p.css('display','');
        m.css('display','flex').find('span').css('display','none');
        s.css('display','flex').find('span').css('display','none');
        for(var i=0;i<d.def.length;i++){
          m.find('.'+d.def[i]).css('display','inline');
          s.find('.'+d.def[i]).css('display','inline');
        }
        if(d.def.length==0){
          $('.menus').css('display','none');
        }
        U.core.openTab=n;
        if(d.click){
          d.click();
        }
      };
      this.gp_update=function(){
        var d=this.TABDATA[this.openTab];
        if(d.click){d.click()}
      };
      $('.panels>div').removeClass('unloaded').css('display','none');
      $('#GP>.tabs p.open').each(vis);
      $('#GP>.tabs p').each(function(){
        $(this).click(function(){
          $('#GP>.tabs p.open').removeClass('open');
          $(this).addClass('open');
          vis.call(this);
        });
      });
      $('#GP>.menus>div>span').click(function(){
        var d=TABDATA[$(this).parent().attr('id').split('_')[0]].menu[$(this).attr('class')];
        if(d.click){
          d.click();
        }
      })
      this.TABDATA=TABDATA;
    };
  }
}
