//U.js
if(!window.hasOwnProperty('U')){!function(){
	var modules=[],proto=function(C,F){
		/*
		Javascript does not allow you implicitly to call an object instance.
		It does, however, allow you to prototype a function.
		Thus, prototype function F with class C.
		Same for any other variable passed as F.
		*/
		var instance,constructor,ret;
		if(C.constructor==Function){ //Is C a class or an instance?
			instance=new C();
			constructor=C;
		}else{
			instance=C;
			constructor=C.constructor;
		}
		if(F.constructor==Function){ //is F a function?
			ret=function(){ //return function value
				return F.apply(ret,arguments) //ret acts as "this", but "F" is actually being run
			};
			ret.__call=F; //allow __call to be referenced internally if this(...) is not supported
		}else{
			ret=F; //if F is static, return that
			if(F.constructor=='object'){
				F=Object.keys(F).reduce(function(a,b){
					a[b]=F[b];
					return a;
				},{})
			}
			ret.__val=F; //allow F to be referenced internally if this[...] is not supported
		}
		for(var element in instance){ //now copy the values over to ret
			!function(element){
				ret[element]=instance[element].constructor==Function?instance[element].bind(ret):instance[element]; //duplicate static attributes
			}(element)
		}
		ret.__parent__=instance; //allow instance reference
		ret.constructor=constructor; //allow constructor reference
		if(ret.hasOwnProperty('__init')){ret.__init()}; //if ret wants things to be done after being prototyped, do that.
		return ret; //return the newly prototyped return function
	},U=proto(function(){ //the class an instance of which goes into the final prototype
		this.__init=function(){ //function to be referenced after call initialized
			if(!this.__call){ //if something went wrong
				this.error('U IS NOT A FUNCTION. THIS MAY CAUSE MODULE ERRORS.');
			}else if(this.prefs.preloadOn){ //if we should get our first few modules
				U=this; //self-prereference in case of UGEN call
				this(this.prefs.preload); //do that
			}
		}
		/*
						************************SPECIAL PREFERENCES************************
		*/
		this.prefs={ //default values
			moduleFolder:'js/mod/', //where the module scripts are stored
			htmlFolder:'html/', //where module html is stored
			cssFolder:'css/', //where module css is stored
			preload:['autoload'], //what modules to preload
			preloadOn:true, //should we preload?
			retryLoad:true, //if we fail to get a module, should we try again?
			errorReportingOn:true //should we log errors to the console, or just send them to the error log?
		};
		/*
						*******************************************************************
		*/
		this.proto=proto; //so other modules can use the proto function
		this.jobs=[]; //cache of things to do once we clarify that we have a specific module
		this.doJobs=function(){ //run through cache
			if(this.atDependencyLevel){return}
			var r;
			for(var i=0;i<this.jobs.length;i++){
				if(U.hasOwnProperty(this.jobs[i][0])){ //check if the module exists
					r=this.jobs[i][1].call(this[this.jobs[i][0]]); //do this job from within the correct module
				}
			}
			this.jobs=[]; //clear the cache
			return r; //in the case of all existing modules, return the value of the last job
		};
		this.ajax=function(url,callback,fail){ //custom AJAX function for internal loading
			x=new XMLHttpRequest(); //create object
			x.onreadystatechange=function(){
				if(x.readyState==4){ //if done
					if(typeof(callback)=='function'&&x.status==200){callback(x.responseText)} //if success
					else if(typeof(fail)=='function'&&x.status!=200){fail()} //if fail
				} //else wait
			}
			x.open('GET',url,true); //Obtain the url using GET.  POST not supported for internal loading.
			x.send(); //send the request to the server
		};
		this.error=function(errorString){ //what to do if you have an error
			this.errorLog.push(errorString); //put it in the error log
			if(this.prefs.errorReportingOn){
				console.log(errorString); //if you're supposed to, log it to the console
			}
		};
		this.errorLog=[]; //error log for developers or anyone wondering why the page isn't working
		this.blank=function(){}; //blank function for referencing in case of callback overload
		this.strStd=function(string){ //standard for module names (no uppercase, no spaces)
			return string.toLowerCase().replace(/^\s\s*|\s\s*$/g,'').replace(/\s/g,'_');
		};
		/*
		U.GEN is a bit complicated, so I'm giving it a bit of a blurb.
		Because of the module structure of U, modules can just reference the parent as "U."
		However, it is bad practice for me to reference a variable from within itself unless that variable is recursive.
		Thus, this.GEN allows me to reference global variables from within a sub-object.
		For instance, if I wanted to duplicate "strStd" into an object called "coolFuncs," this would be the syntax:
			this.coolFuncs={
				strStd:this.GEN.call(this,this.strStd)
			}
		This sets U.coolFuncs.strStd to a generator function which, when called, runs strStd in the main U object.
		Without U.GEN, strStd would be run in U.coolFuncs.
		For strStd, this works fine, but for functions that manipulate variables in U, they need to be pointed to the correct object.
		That's what U.GEN does.
		*/
		this.GEN=function(e){var self=this;return function(){return e.apply(self,arguments)}};
		//Self reference and special case only for U.  Required only for internals.  Avoid using this function.
		this.UGEN=function(e){return function(){return e.apply(U,arguments)}};
		var moduleTemplateFunctions={},
		moduleTemplate=function(object,module){
			object.modName=module.name;
			for(var func in moduleTemplateFunctions){
				object[func]=moduleTemplateFunctions[func];
			}
			this.self=object;
		}
		this.extendModuleTemplate=function(vars){
			moduleTemplateFunctions=Object.keys(vars).reduce(function(c,d){c[d]=vars[d];return c},moduleTemplateFunctions);
		}
		this.newmodule=function(module,onfinish){ //add a module that doesn't already exist to U
			module.proto=typeof(module.proto)=='undefined'?{}:module.proto; //if the module has a special prototype use that
			onfinish=typeof(onfinish)=='undefined'?function(){}:onfinish; //if this is part of a recursion keep doing that
			var finish=this.UGEN(function(){ //when you're done
				/*
				Place the new module in the Global U object.
				As good as U.GEN is, when you add a new module you absolutely must insert it via the variable "U",
				because the different calls for "newmodule" can be very different,
				and by the time you get down to this level,
				there's a strong likelyhood that the U object will be only a clone of the real thing.
				*/
				if(!U.hasOwnProperty(module.name)){ //check the existence of the module
					this.atDependencyLevel=false;
					U.setVal(module.name,new moduleTemplate(proto(module.val,module.proto),module).self); //put the new module into U
					onfinish();
				}
			});
			if(typeof(module.dependencies)!='undefined'){ //If the module has dependencies
				this.atDependencyLevel[module.name]=true;
				return this.addDependencyRecursive(module,finish); //load them first
			} //or else
			finish(); //just end this now
		};
		this.setVal=function(v,f){this[v]=f;} //a slightly more kosher approach than U[x]=y.  Allows conditions to be added later.
		/*
		U works like a library.
		I'm writing only a certain amount of code,
		but it's designed so that if someone needs to come in later and add a new module,
		that's as easy as pie.
		knownLibs is a quick writeup for all the libraries I'm going to use in my prototype.
		*/
		this.knownLibs=[{
			name:'jquery', //name of the reference
			alts:['jq'], //alternate (shorter) names
			ref:'http://code.jquery.com/jquery.min.js', //where the library is
			testVar:'$' //check to make sure this variable is there after you load the script
		},{ //etc.
			name:'chosen',
			ref:'http://harvesthq.github.io/chosen/chosen.jquery.js',
			testVar:'$.fn.chosen'
		},{
			name:'d3',
			ref:'https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js',
			testVar:'d3'
		},{
			name:'maps',
			alts:['google_maps','google_maps_js_api'], //alternate (more thorough) names
			hasCallback:true, //the Google Maps JS API is evil to web systems. I'll explain this later.
			ref:'https://maps.googleapis.com/maps/api/js?v=3.exp&callback=', //see the open equals?  I'll explain this later.
			testVar:'google.maps'
		}];
		this.atDependencyLevel={};
		this.addDependencyRecursive=function(module,after){ //oh, so we're adding dependencies, are we?
			this.atDependencyLevel[module.name]=true; //we're still doing stuff
			var dependencies=module.dependencies; //get the dependency data
			if(dependencies.length==0){return after()} //if you finished loading all of them, run the end function.
			var dependency=this.strStd(dependencies[0]), //the first (current) dependency
				self=this, //U (for moveOn)
				error=this.GEN.call(this,this.error), //for error reporting in-function
				moveOn=function(){
					module.dependencies=dependencies.slice(1); //get rid of the dependency you already added
					self.addDependencyRecursive.apply(self,[module,after]); //iterate to the next recursion
				};
			if(typeof(dependency)=='string'){ //if the user didn't give you an object, but just a string
				if(dependency.substr(dependency.length-3)!='.js'){ //if it doesn't end in js
					for(var i=0;i<this.knownLibs.length;i++){ //check the libraries you know first
						var lib=this.knownLibs[i]; //current library
						if(lib.name==dependency){ //if it's this library
							dependency=lib; //reset the string to everything we know about this library
							break; //don't look at any more libraries
						}else if(lib.hasOwnProperty('alts')){ //check the alternative names for this library
							for(var j=0;j<lib.alts.length;j++){
								var alt=lib.alts[j];
								if(alt==dependency){ //if the user gave you an alt
									dependency=lib; //select this library as the dependency
									break; //stop looking at alt names
								}
							}
							if(typeof(dependency)!='string'){ //if it was an alternative name
								break; //stop looking at libraries
							}
						}
					}
					if(typeof(dependency)=='string'){ //was it not one of the known libraries?
						//assume here that it's a local module (otherwise it should end in .js)
						if(this.hasOwnProperty(dependency)){ //check if it already exists
							moveOn(); //if it does, keep going
						}else{
							this.module.load(dependency,moveOn); //otherwise, load the dependency and then keep going
						}
						return; //you're done here, don't try to load this externally.
					}
				}else{ //okay, so it ends in js.
					dependency={ref:dependency} //Reference the file.  We're done.
				}
			}
			//if dependency was not a string, it is assumed to be an object with a proper referral link.
			var testfunc=function(){ //determine if the testVar does not exist
				if(!dependency.hasOwnProperty('testVar')){return false}
				var root=!window[dependency.testVar]; //basic true/false test if the variable doesn't exist
				if(dependency.testVar.indexOf('.')!=-1){ //however, if the variable is an attribute
					var spl=dependency.testVar.split('.'); //get both the subject variable and all attribute names
					root=!spl.reduce(function(a,b){ //if the variable doesn't exist or any of the attributes doesn't exist
						return a&&a[b]
					},window);
				}
				return root
			};
			if(testfunc()){ //if the dependency already exists, we don't need to load it.  Otherwise...
				/*
				Okay, here's where I talk about the google referrer link.
				The Google Maps JS API is supposed to be referenced as the only script in the header.
				It doesn't support being called by AJAX, and it calls "document.write,"
				which doesn't work for scripts created by JS.
				The only workaround to calling the Google Maps is to provide it with a callback function;
				For some reason or another this leads it to use document.element.append as opposed to the deprecated document.write.
				Why this is I'll never know, but for the time being it requires a callback function.
				Either the user can provide a callback function, or I'll tell it to call my blank function from earlier.
				*/
				if(dependency.hasOwnProperty('hasCallback')){ //If this is the Google Maps JS API
					dependency.ref+=module.hasOwnProperty('callback')?'U.'+module.name+'.'+module.callback:'U.blank'; //Add the callback to the URL
				}
				//If this website uses document.write and actually supports AJAX *cough *cough *google
				if(dependency.hasOwnProperty('mustUseAJAX')&&dependency.mustUseAJAX==true){
					this.ajax(dependency.ref,function(response){ //call it with AJAX
						try{eval(response)} //try and evaluate it
						catch(e){error('COULD NOT LOAD DEPENDENCY FROM "'+dependency.ref+'".  THREW ERROR "'+e+'".')} //or throw an error
						moveOn(); //regardless, keep going.
					},function(){
						error('COULD NOT LOAD DEPENDENCY FROM "'+dependency.ref+'".'); //throw an error
						moveOn(); //regardless, keep going.  I don't believe in second chances for remote servers.
					});
				}else{ //Hooray, a normal situation!
					var script=document.createElement('script'); //create a script element
					script.type="text/javascript"; //tell the browser it's JavaScript
					script.src=dependency.ref; //give it the proper url
					script.onload=function(){ //when it loads
						if(testfunc()){ //enforce the testvar
							setTimeout(function(){ //if it's not there, wait a little while
								if(testfunc()){ //check again
									error('COULD NOT LOAD DEPENDENCY FROM "'+dependency.ref+'".'); //if you really failed, so be it
								}
								moveOn(); //regardless, move on.
							},100);
						}else{
							moveOn(); //ditto.
						}
					}
					document.getElementsByTagName('head')[0].appendChild(script); //put that script in the browser
				}
			}else{
				moveOn(); //hey, we don't need to worry about this dependency!  move on!
			}
		};
		//These make sure no module is handled more than once.  Makes life a lot easier.
		this.modulesHandled=[];
		this.handledModule=function(name){return this.modulesHandled.indexOf(name)!=-1}
		this.handleModule=function(name){return this.handledModule(name)||!this.modulesHandled.push(name)}
		this.module={ //this really should be a module, but that would be a wee bit difficult to pull off.
			buffer:[], //modules to load go in here
			flags:[], //modules that aren't working go in here
			finishJobs:[], //stuff to do at the end goes in here
			load:function(){ //main loader function
				//multiple modules?  one?  figure that out.
				this.buffer=this.buffer.concat(arguments[0].constructor==Array?arguments[0]:[arguments[0]]);
				if(arguments.length==2){ //if they want to tell you to do something else
					this.finishJobs=this.finishJobs.concat(arguments[1]); //do it later. there's work to do.
				}
				this.recursive(); //start the recursion
			},
			/*
			Modules load recursively, a la:
			RECURSIVE
			|AJAX CALL TO FILE1.JS
			||ONLOAD
			|||RECURSIVE
			||||AJAX CALL TO FILE2.JS
			|||||ONLOAD
			||||||RECURSIVE
			|||||||NO MORE, DO END STUFF
			This enforces that no dependent code will be running before its dependencies have been dealt with.
			*/
			url:function(x){
				return this.moduleFolder+x
			},
			recursive:function(){ //recursion handler
				if(this.buffer.length==0){return this.finish()} //if you're done, run the finish function
				try{
					if(this.handledModule(this.buffer[0])){this.success()} //check before you make that ajax call if you don't need to
					if(typeof(this.buffer[0])!='string'){throw new Error('UNSUPPORTED TYPE')}
					this.ajax(this.url(this.buffer[0]+'.js'), //make an ajax call to the first module
						this.GEN.call(this,this.success), //if you succeed, run the success function
						this.GEN.call(this,this.fail))} //if you fail, run the fail function
				catch(e){return this.fail(e)} //if the ajax fails, run the fail function
			},
			finish:function(){
				if(this.finishJobs.length>0){
					for(var i=0;i<this.finishJobs.length;i++){ //if there are jobs to do
						this.finishJobs[i](); //do them
					}
					this.finishJobs=[]; //then clear the buffer for more
				}else{
					this.doJobs(); //now do the main U jobs passed to the modules
				}
			},
			success:function(response){
				/*
				All modules must be of the form
				var module={
					name:'this_module',
					val:function_or_something,
					...
				};
				*/
				if(typeof(this.buffer[0])=='undefined'){this.buffer=this.buffer.slice(1);return} //if the browser deletes elements in a weird way
				if(typeof(this.buffer[0])!='string'){return this.fail('UNSUPPORTED TYPE.')} //if something REALLY weird happened
				if(!this.handleModule(this.buffer[0])){
					var module; //create a test variable called module
					try{eval(response)} //try and redifine "module"
					catch(e){return this.fail('RESOURCE CORRUPTED, THREW ERROR "'+e+'".')} //if invalid JS, run the fail function
					if(typeof(module)!='undefined'){ //if "module" has been redefined
						this.buffer=this.buffer.slice(1); //get rid of the module you just dealt with
						this.handle(module,this.GEN.call(this,this.recursive)); //add it as a new module, and then continue the recursion
					}else{ //if "module" has not been redefined (valid JS, not valid module)
						return this.fail("RESOURCE CORRUPTED, NO MODULE FOUND."); //run the fail function
					}
				}
			},
			fail:function(code){
				code=typeof(code)=='undefined'?'':' '+code; //if additional error string passed, use it
				if(
					code.length>0|| //resource loaded but corrupted in some way
					this.autoFail|| //the user doesn't want you to try again
					(this.flags.length==1&&this.flags[0]==this.buffer[0]) //this is your second try
				){
					this.error('FAILED TO LOAD RESOURCE FROM "'+this.buffer[0]+'".'+code); //throw error
					this.flags=this.flags.slice(1); //get rid of flag
					this.buffer=this.buffer.slice(1); //get rid of the module you failed to deal with
				}else{
					/*
					U defaults to attempt loading its own modules twice,
					because modules tend to be crucial to page execution.
					This is non-standard, but it should provide additional functionality
					in cases where the user has poor internet connections.
					*/
					this.flags.push(this.buffer[0]); //flag the module and try again
				}
				this.recursive(); //no matter what the first module in the buffer is, run the recursion
			},
			/*
			Here's a bunch of functions from U that I've copied into the modules object
			so they can be referenced internally.
			I'm using a combination of this.GEN and raw referencing,
			but IT SHOULD BE NOTED that this is a special case;
			In any other object / module, the user would reference U as "U."
			Because they are external code, that is fine practice,
			however for the sake of this one object, I am duplicating these functions.
			*/
			moduleFolder:this.prefs.moduleFolder,
			handle:this.UGEN(this.newmodule), //handle references the new module function
			error:this.UGEN(this.error),
			doJobs:this.UGEN(this.doJobs),
			ajax:this.UGEN(this.ajax),
			handleModule:this.UGEN(this.handleModule),
			handledModule:this.UGEN(this.handledModule),
			autoFail:!this.prefs.retryLoad, //if the user says "don't retry the load" then "auto fail" is true
			GEN:this.GEN //fails when referenced upon itself.  Doesn't matter what this is because it's re-declared each time.
		};
	},function(){ //the function an instance of which goes into the final prototype
		if(arguments.length==0){ //if they're just calling U
			return this; //return U. You have to return something, so why not something useful?
		}
		var modules,files=[]; //make two empty variables
		if(arguments[0].constructor==Array){ //if the user passed U(['MOD1','MOD2'])
			modules=arguments[0]; //deal with all those
		}else if(arguments[0].constructor==String){ //if the user passed U('MOD')
			modules=[arguments[0]]; //deal with just that, but in an array for uniformity
		}
		if(typeof(modules)!='undefined'){ //make sure the user didn't pass something else weird
			for(var i=0;i<modules.length;++i){ //for all the passed modules
				modules[i]=this.strStd(modules[i]); //reformat the string to module name standard
				//if this is a file we need to get, add the file form of the module to the files array
				if(!this.hasOwnProperty(modules[i])){files.push(modules[i])}
			}
			if(arguments.length==2){ //if the user wants to do something with these modules, pronto
			/*
			One of the features that I provide in U is the following:
				U('MODULE',function(){
					this.method(...);
					this.attribute=value;
					...
				})
			MODULE is either found in U or loaded (it works the same both ways)
			and then a function is run within the scope of the MODULE.
			"this" refers to the fully loaded MODULE.
			SPECIAL NOTE:
			Be careful how you use this functionality if you require dependencies
			that load their own sub-dependencies, like the Google Maps JS API.
			In this case, it's best to write the bulk of your live code in the given callback function.
			*/
				var jobs=[]; //this will be put in U.jobs later
				if(typeof(arguments[1])=='object'){ //if there's a bunch of modules
					for(var i in arguments[1]){ //for everything in the object
						/*
						OK, also a little complicated, so another little blurb.
						These two lines of code support two different functionalities:
						U(['MOD1','MOD2'],[function_for_mod1,function_for_mod2])
						and
						U(['MOD1','MOD2'],{MOD1:function_for_mod1,MOD2:function_for_mod2})
						If it's an array, it is assumed you ordered the functions in the same order you placed the modules.
						If it's an object, nothing is assumed.
						*/
						if(String(+i)==i){ //if the index is a number, get the respective string from "modules"
							jobs.push([modules[i],arguments[1][i]]);
						}else{
							jobs.push([i,arguments[1][i]]); //add an array with the index and module usage function
						}
					}
				}else{ //There's only one job
					//assume it's for the last passed module if there's more than one module
					jobs.push([modules[modules.length-1],arguments[1]]); //put that in jobs
				}
				this.jobs=this.jobs.concat(jobs); //put the temporary jobs array in the real jobs array
			}else if(files.length==0){return this[modules[modules.length-1]]}
			if(files.length!=0){ //are there modules that have to be loaded?
				this.module.load(files); //load those files (and then run the jobs you have in store for those files)
			}else{ //all the necessary modules already exist in U
				return this.doJobs(); //run the jobs immediately
			}
		}
	});
	window['U']=U; //You're done prototyping!  Store your system and get to work loading some content!
}()}
/*
SUMMARY OF FUNCTIONALITY ( U ) {
	1.	U('MODULE NAME')
		-> loads "module_name" from the folder specified in U.prefs.moduleFolder or enforces that it already exists.
	2.	U(['MOD1','MOD2'])
		-> loads both "mod1" and "mod2".
	3.	U('Module',function(){
			this.method(this.attribute);
			...
		})
		-> if it doesn't already exist, loads "module" and then runs a function within the scope of "module."
	4a.	U(['MOD1','MOD2'],{MOD1:function(){
			...
		},MOD2:function(){
			...
		}})
		OR
	4b.	U(['MOD1','MOD2'],[function(){
			... //for MOD1
		},function(){
			... //for MOD2
		}])
		-> enforces the existence of "mod1" and "mod2" and then runs functions within them.
	5.	U(['MOD1','MOD2'],function(){
			... //for MOD2
		})
		-> runs a function in the last passed module
	6.	U.module_name.method(...)
		OR
		U('Module Name').method(...)
		-> runs a method of a loaded module
	7.	U.module_name.attribute=value
		OR
		U('MODULE_name').attribute=value
		-> sets the value of an attribute of a loaded module
	8.	U.method(...)
		-> runs a method of the main object
}
*/
