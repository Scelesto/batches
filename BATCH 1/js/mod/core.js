module={
  name:'core',
  dependencies:['jq','css','usage','chart','conditions','svg','logic','map'],
  val:function(){
    this.onload=function(){
      U('chosen',function(){
        this('.chosen-select',{
          max_selected_options:1
        });
      });
      //interactive js
      var TABDATA={
        qnet:{
          menu:{
            rem_qst:{

            },cln_qst:{

            },lss_lnk:{

            },mre_lnk:{

            },edt_lbl:{

            },ans_qst:{

            },hde_box:{

            },hde_lnk:{

            },clr_pnl:{

            },org_box:{

            },shw_trc:{

            },fnc_qst:{

            }
          },sidebar:{
            add_qst:{

            },
            spc_trk:{

            }
          },def:[
            'clr_pnl',
            'add_qst',
            'spc_trk'
          ],click:function(){

          }
        },logic:{
          def:[]
        },time:{
          def:[]
        },sex:{
          def:[]
        },age:{
          def:[]
        },location:{
          def:[]
        },settings:{
          def:[]
        }
      }
      var vis=function(){
        $('.menus').css('display','');
        var i=$(this).attr('id'),n=i.split('_')[0],d=TABDATA[n],m=$('#'+n+'_menu'),p=$('#'+n+'_panel'),s=$('#'+n+'_sidebar');
        $('.menus>div').css('display','none');$('.panels>div').css('display','none');
        p.css('display','');
        m.css('display','flex').find('span').css('display','none');
        s.css('display','flex').find('span').css('display','none');
        for(var i=0;i<d.def.length;i++){
          m.find('.'+d.def[i]).css('display','inline');
          s.find('.'+d.def[i]).css('display','inline');
        }
        if(d.def.length==0){
          $('.menus').css('display','none');
        }
      }
      $('.panels>div').removeClass('unloaded').css('display','none');
      $('#GP>.tabs p.open').each(vis);
      $('#GP>.tabs p').each(function(){
        $(this).click(function(){
          $('#GP>.tabs p.open').removeClass('open');
          $(this).addClass('open');
          vis.call(this);
        });
      });
    };
  }
}
