module={
  name:"autoload",
  dependencies:['jq'],
  val:function(){
    U.extendModuleTemplate({
      html:function(element){
        var onload=this.onload?this.onload:function(){};
        $.get(U.prefs.htmlFolder+this.modName+".html",function(data){
          element.html(data);
          onload();
        });
      },
      css:function(){
        //test if css file exists
        var u=U.prefs.cssFolder+this.modName+".css",x=new XMLHttpRequest();
        x.onreadystatechange=function(){
          if(x.readyState==4&&x.status==200){ //then load the css file
            $('<link/>',{rel:"stylesheet",type:"text/css",href:u}).appendTo('head');
          }
        }
        x.open('HEAD',u);
        x.send();
      }
    });
    var modFills=[];
    $('[data-module-fill]').each(function(){
      modFills.push($(this))
      var $this=$(this);
    });
    U(modFills.map(function(x){return x.data('module-fill')}),modFills.map(function(x){return function(){
      this.html(x);
      this.css();
    }}));
  }
}
