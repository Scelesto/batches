module={
  name:'rqb',
  dependencies:['jq'],
  val:function(){
    this.onload=function(){
      $('[data-module-fill="rqb"]>img').click(function(){
        $('#rqb_overlay').show();
        $('#rqb_overlay input').each(function(){this.checked=false});
      });
      $('#rqb_overlay').click(function(){
        if(!$('#rqb_box').is(':hover')){
          $(this).hide();
        }
      });
    }
  }
}
